/*CALCULADORA*/
function calculo(n1,n2,accion){
    const operaciones = ['suma','resta','multiplicacion','division']
    
    let result = operaciones.indexOf(accion)
    let total =''
        if (result == [0]){
           total =(n1 + n2)
        }else if (result == [1]){
            total=(n1 - n2)
        }else if (result == [2]){
            total=(n1 * n2)
        }else if (result == [3]){
            total= (n1 / n2)
        }
        return total.toFixed(2)
    }
    
    console.log(calculo(4.35,3,'multiplicacion'))